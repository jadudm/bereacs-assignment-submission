import argparse
from peewee import *

db = SqliteDatabase('test.db')

courses = ['CSC111', 'CSC222']

class Submission(Model):
  sid = PrimaryKeyField()
  username = TextField()
  timestmap = DateField()
  class Meta:
    database = db

def initDB():
  db.connect()
  db.create_tables([Submission], safe = True)

def handleSubmission (args):
  

if __name__ == "__main__":
  initDB()
  parser = argparse.ArgumentParser(prog = 'submit',
                                   description = 'Berea CS File Submission')

  parser.add_argument('course', choices = courses,
                      type=str,
                      help = 'The course or activity you are submitting to; pick one.')

  parser.add_argument('assignID', type = str,
                      help = 'The assignment ID.')

  parser.add_argument('fileOrDir', type = str,
                      help = 'The file or directory you are submitting.')


  args = parser.parse_args()

  handleSubmission(args)
